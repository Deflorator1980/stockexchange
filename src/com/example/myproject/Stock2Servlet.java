package com.example.myproject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.StringTokenizer;

import javax.servlet.http.*;

@SuppressWarnings("serial")
public class Stock2Servlet extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("text/html; charset=windows-1251");
		PrintWriter pw = response.getWriter();
		String company = request.getParameter("comment");
		String csv;
		URL url = null;
		URLConnection urlCon = null;
		InputStreamReader inStream = null;
		BufferedReader buff = null;
		try{
			url = new URL("http://quote.yahoo.com/d/quotes.csv?s="
                   + company + "&f=sl1d1t1c1ohgv&e=.csv");
			urlCon = url.openConnection();
			inStream = new InputStreamReader(urlCon.getInputStream());
			buff = new BufferedReader(inStream);
			
			csv = buff.readLine();
			StringTokenizer tokenizer = new StringTokenizer(csv, ",");
			String ticker = tokenizer.nextToken();
			String price = tokenizer.nextToken();
			String tradeDate = tokenizer.nextToken();
			String tradeTime = tokenizer.nextToken();			
			pw.println("Company: " + ticker + " Price: " + price + " Date: " + tradeDate + " Time: " + tradeTime);
		}catch (Exception e){e.printStackTrace();}
		finally{
			try{
				inStream.close();
				buff.close();
		}catch(Exception e){e.printStackTrace();}
	}
	}
}
